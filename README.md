# ms-coberturas

This project uses Quarkus, the Supersonic Subatomic Java Framework.

If you want to learn more about Quarkus, please visit its website: https://quarkus.io/ .

## Running the application in dev mode

You can run your application in dev mode that enables live coding using:
```shell script
./mvnw compile quarkus:dev
```

> **_NOTE:_**  Quarkus now ships with a Dev UI, which is available in dev mode only at http://localhost:8080/q/dev/.

### Basic Requirements

To compile and run this project, ensure you have the following prerequisites installed:

- **Java:** Version 17 or higher. You can download and install Java from [the official Oracle website](https://www.oracle.com/java/technologies/javase-jdk17-downloads.html) or use an OpenJDK distribution.

- **GraalVM for Java:** Version 17. GraalVM is an Oracle JVM implementation that includes the Graal compiler, enabling Ahead-Of-Time (AOT) compilation to create native binaries. Obtain GraalVM from [its download page](https://www.graalvm.org/downloads/).

- **Maven:** Version 3.9 or higher. Maven is an open-source project management tool primarily used for building and managing Java projects. You can download Maven from [the official Apache Maven website](https://maven.apache.org/download.cgi).

- **Docker:** Docker is a platform that facilitates development, shipping, and running applications within containers. Ensure Docker is installed on your system. You can find installation instructions on [the Docker website](https://docs.docker.com/get-docker/).

### Configuration Instructions

1. **Installation of Requirements:**
    - Install Java 17 following the instructions provided by Oracle or using an OpenJDK distribution.
    - Download and install GraalVM for Java 17 following the instructions available on its website.
    - Ensure you have Maven 3.9 or higher installed.
    - Install Docker following the instructions provided in the official documentation.
## Creating a native executable

You can create a native executable using: 
```shell script
./mvnw package -Dnative
```

Or, if you don't have GraalVM installed, you can run the native executable build in a container using: 
```shell script
./mvnw package -Dnative -Dquarkus.native.container-build=true
```

You can then execute your native executable with: `./target/ms-coberturas-1.0.0-SNAPSHOT-runner`

If you want to learn more about building native executables, please consult https://quarkus.io/guides/maven-tooling.

## Related Guides

- REST ([guide](https://quarkus.io/guides/rest)): A Jakarta REST implementation utilizing build time processing and Vert.x. This extension is not compatible with the quarkus-resteasy extension, or any of the extensions that depend on it.
- Hibernate ORM ([guide](https://quarkus.io/guides/hibernate-orm)): Define your persistent model with Hibernate ORM and Jakarta Persistence
- REST Jackson ([guide](https://quarkus.io/guides/rest#json-serialisation)): Jackson serialization support for Quarkus REST. This extension is not compatible with the quarkus-resteasy extension, or any of the extensions that depend on it
- JDBC Driver - MySQL ([guide](https://quarkus.io/guides/datasource)): Connect to the MySQL database via JDBC
- JDBC Driver - Oracle ([guide](https://quarkus.io/guides/datasource)): Connect to the Oracle database via JDBC

## Provided Code

# QueryBuilder

This class provides utilities for building and executing dynamic SQL queries in a database, allowing the execution of parameterized queries and obtaining results mapped to Java objects.

## Features

- Builds dynamic SQL queries.
- Executes parameterized queries.
- Facilitates result mapping to Java objects.
- Compatible with selection (SELECT) and modification (INSERT, UPDATE, DELETE) queries.

## Basic Usage

### Obtaining an Object

```java
// Create an instance of QueryBuilder with a DataSource and the mapped entity class
QueryBuilder queryBuilder = new QueryBuilder(dataSource, EntityClass.class);

// Define the query type
queryBuilder.queryType(QueryType.SELECT);

// Define the SQL query
queryBuilder.query("SELECT * FROM table WHERE column = :parameter");

// Add parameters to the query
queryBuilder.addParameter("parameter", value);

// Get results mapped to objects
EntityClass result = (EntityClass) queryBuilder.getObject();
```


### Obtaining an Collection

```java
// Create an instance of QueryBuilder with a DataSource and the mapped entity class
QueryBuilder queryBuilder = new QueryBuilder(dataSource, EntityClass.class);

// Define the query type
queryBuilder.queryType(QueryType.SELECT);

// Define the SQL query
queryBuilder.query("SELECT * FROM table WHERE column = :parameter");

// Add parameters to the query
queryBuilder.addParameter("parameter", value);

// Get results mapped to objects
List<EntityClass> result = (List<EntityClass>) queryBuilder.getList();
```

### Using List as parameter

```java
// Create an instance of QueryBuilder with a DataSource and the mapped entity class
QueryBuilder queryBuilder = new QueryBuilder(dataSource, EntityClass.class);

// Define the query type
queryBuilder.queryType(QueryType.SELECT);

// Define the SQL query
queryBuilder.query("SELECT * FROM table WHERE column in (:parameters)");

// Add parameters to the query
queryBuilder.addParameter("parameters", List.of(value1, value2, value3));

// Get results mapped to objects
List<EntityClass> result = (List<EntityClass>) queryBuilder.getList();
```


### Using Map to input all parameters at once.

```java
// Create an instance of QueryBuilder with a DataSource and the mapped entity class
QueryBuilder queryBuilder = new QueryBuilder(dataSource, EntityClass.class);

// Define the query type
queryBuilder.queryType(QueryType.SELECT);

// Define the SQL query
queryBuilder.query("SELECT * FROM table WHERE column1 = parameter1 AND column2 = parameter2");

// Add parameters to the query
queryBuilder.addParameter(Map.of(
        "parameter1", value1,
        "parameter2", value2
));

// Get results mapped to objects
List<EntityClass> result = (List<EntityClass>) queryBuilder.getList();
```


### Modifying/Creating data

```java
// Create an instance of QueryBuilder with a DataSource and the mapped entity class
QueryBuilder queryBuilder = new QueryBuilder(dataSource, EntityClass.class);

// Define the query type
queryBuilder.queryType(QueryType.INSERT);

// Define the SQL query
queryBuilder.query("INSERT INTO table (id, msg) VALUES (:id , :msg);");

// Add msg parameter to the query
queryBuilder.addParameter("msg", msgValue);

// Add id parameter to the query
queryBuilder.addParameter("id", idValue);

// Implement the changes
queryBuilder.execute();
```


## Additional Notes
- Classes to be mapped must have the following annotations and structure:

```java
import org.bicevida.infrastructure.adapters.output.database.utils.annotations.ColumnName;
import org.bicevida.infrastructure.adapters.output.database.utils.annotations.MappedEntity;


@MappedEntity
public class EntityClass {

    @ColumnName(value = "id")
    private Long id;

    @ColumnName(value = "msg")
    private String msg;
}
```


package org.bicevida.infrastructure.adapters.output.database.utils.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.bicevida.infrastructure.adapters.output.database.utils.anotations.ColumnName;
import org.bicevida.infrastructure.adapters.output.database.utils.anotations.MappedEntity;

@Setter
@Getter
@NoArgsConstructor
@MappedEntity
public class TestEntity {

    @ColumnName(value = "id")
    private Long id;

    @ColumnName(value = "msg")
    private String msg;

}

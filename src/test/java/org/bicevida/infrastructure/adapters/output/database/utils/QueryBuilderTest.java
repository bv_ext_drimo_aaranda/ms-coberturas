package org.bicevida.infrastructure.adapters.output.database.utils;

import io.quarkus.test.junit.QuarkusTest;
import org.bicevida.infrastructure.adapters.output.database.utils.entities.TestEntity;
import org.bicevida.infrastructure.adapters.output.database.utils.enums.QueryType;
import org.bicevida.infrastructure.adapters.output.database.utils.exceptions.MissingParameterException;
import org.bicevida.infrastructure.adapters.output.database.utils.exceptions.NonBuildableEntityException;
import org.bicevida.infrastructure.adapters.output.database.utils.exceptions.QueryStatementException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import javax.sql.DataSource;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;


@QuarkusTest
public class QueryBuilderTest {

    private DataSource dataSource;
    private ResultSet resultSet;
    private Connection connection;

    @BeforeEach
    public void setUp() throws SQLException {
        dataSource = Mockito.mock(DataSource.class);
        resultSet = Mockito.mock(ResultSet.class);
        connection = Mockito.mock(Connection.class);
        CallableStatement statement = Mockito.mock(CallableStatement.class);

        when(dataSource.getConnection()).thenReturn(connection);
        when(connection.prepareCall(anyString())).thenReturn(statement);
        when(statement.executeQuery()).thenReturn(resultSet);

        doNothing().when(statement).setString(anyInt(), anyString());
        doNothing().when(statement).setLong(anyInt(), anyLong());
        doNothing().when(statement).setDate(anyInt(), any());
        doNothing().when(statement).setBoolean(anyInt(), anyBoolean());
        doNothing().when(statement).setObject(anyInt(), any());

    }

    @Test
    void queryBuilderWhenOneParameterGetObject_Ok() throws Exception {

        Long testId = 1L;
        String testMessage = "this is a test message";
        TestEntity testObject = null;
        when(resultSet.next())
                .thenAnswer(invocation -> true)
                .thenAnswer(invocation -> false);;
        when(resultSet.getLong(anyString()))
                .thenAnswer(invocation -> testId);
        when(resultSet.getString(anyString()))
                .thenAnswer(invocation -> testMessage);


        QueryBuilder queryBuilder = new QueryBuilder(dataSource, TestEntity.class);
        testObject = (TestEntity) queryBuilder.setQueryType(QueryType.SELECT)
                .query("select id, msg from mockDatabase where id = :id")
                .addParameter("id", 1)
                .getObject();


        assertNotNull(testObject);
        assertEquals(testId, testObject.getId());
        assertEquals(testMessage, testObject.getMsg());
        verify(connection, times(1)).prepareCall("select id, msg from mockDatabase where id = ?");

    }


    @Test
    void queryBuilderWhenOneListParameterGetList_Ok() throws Exception {

        Long testId = 1L;
        String testMessage = "this is a test message 1";
        Long testId2 = 2L;
        String testMessage2 = "this is a test message 2";

        List<TestEntity> testObjectList;
        when(resultSet.next())
                .thenAnswer(invocation -> true)
                .thenAnswer(invocation -> true)
                .thenAnswer(invocation -> false);
        when(resultSet.getLong(anyString()))
                .thenAnswer(invocation -> testId)
                .thenAnswer(invocation -> testId2);
        when(resultSet.getString(anyString()))
                .thenAnswer(invocation -> testMessage)
                .thenAnswer(invocation -> testMessage2);


        QueryBuilder queryBuilder = new QueryBuilder(dataSource, TestEntity.class);
        testObjectList = (List<TestEntity>) queryBuilder.setQueryType(QueryType.SELECT)
                .query("select id, msg from mockDatabase where id in (:ids)")
                .addParameter("ids", List.of(1,2))
                .getList();

        assertNotNull(testObjectList);
        assertFalse(testObjectList.isEmpty());
        assertEquals(2, testObjectList.size());
        verify(connection, times(1)).prepareCall("select id, msg from mockDatabase where id in (?, ?)");

        assertEquals(testId, testObjectList.get(0).getId());
        assertEquals(testMessage, testObjectList.get(0).getMsg());

        assertEquals(testId2, testObjectList.get(1).getId());
        assertEquals(testMessage2, testObjectList.get(1).getMsg());
    }


    @Test
    void queryBuilderWhenOneIncorrectParameterGetObject_Error() throws Exception {

        QueryBuilder queryBuilder = new QueryBuilder(dataSource, TestEntity.class);

        assertThrows(MissingParameterException.class, () -> queryBuilder.setQueryType(QueryType.SELECT)
                .query("select id, msg from mockDatabase where id = :id")
                .addParameter("notiId", 1)
                .execute());

    }


    @Test
    void queryBuilderWhenOneParameterGetObjectIncorrectQueryType_Error() throws Exception {


        QueryBuilder queryBuilder = new QueryBuilder(dataSource, TestEntity.class);

        assertThrows(QueryStatementException.class, () -> queryBuilder.setQueryType(QueryType.INSERT)
                .query("select id, msg from mockDatabase where id = :id")
                .addParameter("id", 1)
                .getObject());

    }

    @Test
    void queryBuilderWhenOneParameterGetObjectNonBuildableClass_Error() throws Exception {

        assertThrows(NonBuildableEntityException.class, () -> new QueryBuilder(dataSource, Object.class));

    }



}

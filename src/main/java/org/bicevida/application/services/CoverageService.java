package org.bicevida.application.services;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import org.bicevida.domain.models.Coverage;
import org.bicevida.domain.ports.output.BdpOutputPort;
import org.bicevida.domain.ports.output.OpenPlanOutputPort;
import org.bicevida.domain.ports.output.SysoneOutputPort;

import java.util.Collections;
import java.util.List;

import static org.bicevida.infrastructure.Constants.MARK_SEC;
import static org.bicevida.infrastructure.Constants.VULCANO_POLICY_MIN_RAGE;

@ApplicationScoped
public class CoverageService {

    private final OpenPlanOutputPort openPlanOutputPort;
    private final SysoneOutputPort sysoneOutputPort;
    private final BdpOutputPort bdpOutputPort;
    @Inject
    public CoverageService(OpenPlanOutputPort openPlanOutputPort, SysoneOutputPort sysoneOutputPort, BdpOutputPort bdpOutputPort){
        this.openPlanOutputPort = openPlanOutputPort;
        this.sysoneOutputPort = sysoneOutputPort;
        this.bdpOutputPort = bdpOutputPort;
    }

    public List<Coverage> getPolicyCoverages(Integer policyNumber, Integer policyPrefix, Integer policySequence) {

        List<Coverage> policyCoverages;

        if (policyNumber >= VULCANO_POLICY_MIN_RAGE) {
            policyCoverages = sysoneOutputPort.getPolicyCoverages(policyPrefix, policyNumber);
        } else {
            policyCoverages = bdpOutputPort.getIndividualPolicyCoverages(policyPrefix, policyNumber);
            policyCoverages.addAll(bdpOutputPort.getCollectivePolicyCoverages(policyPrefix, policyNumber, policySequence));
            policyCoverages.addAll(getIndividualPlusPolicyCoverages(policyPrefix, policyNumber, policySequence));

            Long planId = bdpOutputPort.getPlanId(policyNumber);
            policyCoverages.addAll(openPlanOutputPort.getPolicyCoverages(planId));
        }

        return policyCoverages;

    }


    private List<Coverage> getIndividualPlusPolicyCoverages(Integer policyNumber, Integer policyPrefix, Integer policySequence){

            List<Coverage> policyCoverages;
            if (bdpOutputPort.getIndividualPlusPolicyMark(policyNumber) == MARK_SEC) {
                policyCoverages = bdpOutputPort.getMark2IndividualPolicyCoverages(policyPrefix, policyNumber);
            } else {
                policyCoverages = bdpOutputPort.getMark5IndividualPolicyCoverages(policyPrefix, policyNumber);
            }
            if(policyCoverages
                    .stream()
                    .anyMatch(coverage -> coverage.getInsuredAmount() == 0.0)) {
                return Collections.EMPTY_LIST;
            }
            return policyCoverages;

    }
}

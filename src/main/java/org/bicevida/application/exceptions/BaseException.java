package org.bicevida.application.exceptions;
import jakarta.ws.rs.core.Response.Status;

public abstract class BaseException extends Exception {
    private Status httpStatus;

    public BaseException(String message, Status httpStatus){
        super(message);
        this.httpStatus = httpStatus;

    }
}

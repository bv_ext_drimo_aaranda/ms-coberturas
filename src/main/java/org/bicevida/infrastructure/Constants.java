package org.bicevida.infrastructure;

public class Constants {


    //query parameters
    public static final String POLICY_NUMBER = "policyNumber";
    public static final String POLICY_PREFIX = "policyPrefix";
    public static final String POLICY_SEQUENCE = "policySequence";
    public static final String PLAN_ID = "planId";


    public static int VULCANO_POLICY_MIN_RAGE = 3000000;

    public static int MARK_SEC = 2;


    public static final String OPENPLAN_DATASOURCE = "openPlan";
    public static final String SYSONE_DATASOURCE = "sysone";

}

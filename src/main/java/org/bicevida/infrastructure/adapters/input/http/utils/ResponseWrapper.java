package org.bicevida.infrastructure.adapters.input.http.utils;

import lombok.Builder;

import java.io.Serializable;

@Builder
public class ResponseWrapper implements Serializable {

    private int httpCode;
    private String description;
    private Object data;
}

package org.bicevida.infrastructure.adapters.input.http;

import jakarta.inject.Inject;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import org.bicevida.application.services.CoverageService;
import org.bicevida.domain.ports.input.CoverageInputPort;

public class CoverageController implements CoverageInputPort {

    private final CoverageService coverageService;

    @Inject
    public CoverageController(CoverageService coverageService){
        this.coverageService = coverageService;
    }


    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/v1/coberturas/{policyNumber}/{policyPrefix}/{policySequence}")
    @Override
    public Object getPolicyCoverages(@PathParam("policyNumber") Integer policyNumber,
                                     @PathParam("policyPrefix") Integer policyPrefix,
                                     @PathParam("policySequence") Integer policySequence) {
        try {
            return Response
                    .accepted()
                    .entity(coverageService.getPolicyCoverages(policyPrefix, policyNumber, policySequence));
        } catch (Exception e) {

            return Response
                    .serverError()
                    .entity(e);
        }
    }
}

package org.bicevida.infrastructure.adapters.output.database.utils.exceptions;

import org.bicevida.application.exceptions.BaseException;

import static jakarta.ws.rs.core.Response.Status.INTERNAL_SERVER_ERROR;


public class QueryStatementException extends BaseException {
    public QueryStatementException(){
        super("Query statement does not match query type", INTERNAL_SERVER_ERROR);
    }


}


package org.bicevida.infrastructure.adapters.output.database.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.bicevida.infrastructure.adapters.output.database.utils.anotations.ColumnName;
import org.bicevida.infrastructure.adapters.output.database.utils.anotations.MappedEntity;

@Setter
@Getter
@NoArgsConstructor
@MappedEntity
public class CoverageEntity {

    @ColumnName(value = "coverageTypeId")
    private Long coverageTypeId;

    @ColumnName(value = "benefit")
    private String benefit;

    @ColumnName(value = "insuredAmount")
    private Double insuredAmount;

}

package org.bicevida.infrastructure.adapters.output.database.utils.enums;

public enum QueryType {
    SELECT("select"),
    INSERT("insert"),
    UPDATE("update"),
    DELETE("delete");

    private final String statement;

    QueryType(String statement){
        this.statement = statement;
    }

    public String getStatementName(){
        return this.statement;
    }

}

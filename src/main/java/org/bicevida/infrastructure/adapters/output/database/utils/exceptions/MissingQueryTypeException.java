package org.bicevida.infrastructure.adapters.output.database.utils.exceptions;

import org.bicevida.application.exceptions.BaseException;

import static jakarta.ws.rs.core.Response.Status.INTERNAL_SERVER_ERROR;


public class MissingQueryTypeException extends BaseException {
    public MissingQueryTypeException(){
        super("Query type must be specified", INTERNAL_SERVER_ERROR);
    }


}

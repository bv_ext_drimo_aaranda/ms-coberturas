package org.bicevida.infrastructure.adapters.output.database.utils.exceptions;

import org.bicevida.application.exceptions.BaseException;

import static jakarta.ws.rs.core.Response.Status.INTERNAL_SERVER_ERROR;


public class NonTransactionalException extends BaseException {
    public NonTransactionalException(){
        super("Only non transactional queries ar able to retreat information", INTERNAL_SERVER_ERROR);
    }


}

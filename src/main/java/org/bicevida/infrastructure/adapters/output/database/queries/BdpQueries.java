package org.bicevida.infrastructure.adapters.output.database.queries;

public class BdpQueries {
    public static final String GET_POLICY_MARK =
            "SELECT con.marca AS markNumber " +
                    "FROM masindiv.msi_contrato con " +
                    "WHERE con.con_id_contrato = " +
                    "   (SELECT pol.con_id_contrato " +
                    "    FROM masindiv.msi_Poliza pol " +
                    "    WHERE pol.pol_Numero = :policyNumber )";

    public static final String GET_MARK2_INDIVIDUAL_POLICY_COVERAGE =
            "SELECT " +
                    "   cob.cob_id_cobertura AS coverageTypeId, " +
                    "   TRIM(cob.cob_nombre_largo) AS benefit, " +
                    "   ppc.capital_uf AS insuredAmount " +
                    "FROM " +
                    "   masindiv.msi_coberturas_plan cobplan, " +
                    "   masindiv.msi_coberturas cob, " +
                    "   masindiv.msi_planes pl, " +
                    "   masindiv.msi_plan_poliza_sec pp, " +
                    "   masindiv.msi_poliza_plan_cobertura ppc " +
                    "WHERE " +
                    "   cobplan.plan_id_plan = pl.pla_id_plan " +
                    "   AND cob.cob_id_cobertura = cobplan.cob_id_cobertura " +
                    "   AND pp.pla_id_plan = pl.pla_id_plan " +
                    "   AND pp.pol_prefijo_poliza = :policyPrefix " +
                    "   AND pp.pol_numero_poliza = :policyNumber " +
                    "   AND ppc.numero_poliza = :policyNumber " +
                    "   AND pp.secuencia_poliza = :policySequence " +
                    "   AND ppc.id_Cobertura = cob.cob_id_cobertura " +
                    "   AND ppc.id_Plan = pp.pla_id_plan";

    public static final String GET_MARK5_INDIVIDUAL_POLICY_COVERAGE =
            "SELECT " +
                    "   cob.cob_id_cobertura AS coverageTypeId, " +
                    "   TRIM(cob.cob_nombre_largo) AS benefit, " +
                    "   cobplan.cop_capital_uf AS insuredAmount " +
                    "FROM " +
                    "   masindiv.msi_coberturas_plan cobplan, " +
                    "   masindiv.msi_coberturas cob, " +
                    "   masindiv.msi_planes pl, " +
                    "   masindiv.msi_plan_poliza pp " +
                    "WHERE " +
                    "   cobplan.plan_id_plan = pl.pla_id_plan " +
                    "   AND cob.cob_id_cobertura = cobplan.cob_id_cobertura " +
                    "   AND pp.pla_id_plan = pl.pla_id_plan " +
                    "   AND pp.pol_prefijo_poliza = :policyPrefix " +
                    "   AND pp.pol_numero_poliza = :policyNumber";

    public static final String GET_ID_PLAN = "SELECT cp.planId " +
            "FROM masindiv.msi_poliza p, masindiv.msi_plan_poliza pp, masindiv.msi_coberturas_plan cp " +
            "WHERE p.pol_numero = pp.POL_NUMERO_POLIZA " +
            "AND cp.plan_id_plan = pp.PLA_ID_PLAN " +
            "AND p.pol_numero = :policyNumber ";


    public static final String GET_INDIVIDUAL_POLICY_COVERAGE =
            "SELECT " +
                    "   C.CVCOB AS ID_COBERTURA, " +
                    "   TRIM(C.NOMBRECOB) AS PRESTACION, " +
                    "   CASE C.NOMBRECOB " +
                    "       WHEN 'ASISTENCIA EN VIAJE' THEN '' " +
                    "       ELSE TRIM(C.SA) " +
                    "   END CAPITAL_ASEGURADO " +
                    "FROM " +
                    "   INDIVIDUALES.MS_POLIZA P, " +
                    "   INDIVIDUALES.MS_COBERTURAS C, " +
                    "   INDIVIDUALES.MS_CODPOLCAD CP " +
                    "WHERE " +
                    "   P.RAMSUBRAMO = :policyPrefix " +
                    "   AND P.NPOLIZA = :policyNumber " +
                    "   AND P.RAMSUBRAMO = C.RAMSUBRAMO " +
                    "   AND P.NPOLIZA = C.NPOLIZA " +
                    "   AND P.RAMSUBRAMO = CP.RAMSUBRAMO " +
                    "   AND CP.CVCOB = C.CVCOB " +
                    "ORDER BY " +
                    "   C.TIPOADIC";

    public static final String GET_COLLECTIVE_POLICY_COVERAGES =
            "SELECT " +
                    "    a.codigo_cobertura AS ID_COBERTURA, " +
                    "    TRIM(b.nombre_largo) AS PRESTACION, " +
                    "    a.capital_o_renta AS CAPITAL_ASEGURADO " +
                    "FROM " +
                    "    colectivo.planes a " +
                    "INNER JOIN " +
                    "    colectivo.coberturas b " +
                    "ON " +
                    "    a.codigo_cobertura = b.codigo_cobertura " +
                    "WHERE " +
                    "    a.numero_poliza = :policyNumber " +
                    "    AND a.codigo_prefijo_poliza = :policyPrefix " +
                    "    AND a.secuencia_poliza = :policySequence " +
                    "UNION " +
                    "SELECT " +
                    "    b.cobertura AS ID_COBERTURA, " +
                    "    c.descripcion AS PRESTACION, " +
                    "    b.gasto_maximo AS CAPITAL_ASEGURADO " +
                    "FROM " +
                    "    salud.polizas a " +
                    "INNER JOIN " +
                    "    salud.Coberturas_Contratadas_New b " +
                    "ON " +
                    "    a.poliza = b.poliza " +
                    "INNER JOIN " +
                    "    salud.coberturas_new c " +
                    "ON " +
                    "    b.cobertura = c.cobertura " +
                    "WHERE " +
                    "    a.pol_numero = :policyNumber " +
                    "    AND a.pol_prefijo = :policyPrefix " +
                    "    AND a.pol_secuencia = :policySequence";




}

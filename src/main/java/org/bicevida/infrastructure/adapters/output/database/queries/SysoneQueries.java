package org.bicevida.infrastructure.adapters.output.database.queries;

public class SysoneQueries {
    public static final String GET_POLICY_COVERAGES =
            "SELECT " +
                    "    ic.TYPE_ID AS ID_COBERTURA, " +
                    "    ic.DESCRIPTION AS PRESTACION, " +
                    "    pcv.INSURED_AMOUNT AS CAPITAL_ASEGURADO " +
                    "FROM " +
                    "    CONTRACT_HEADER B " +
                    "LEFT JOIN " +
                    "    COVERAGE_PLAN cp ON cp.COVERAGE_PLAN_SECTION_ID = B.SECTION_ID " +
                    "    AND cp.COVERAGE_PLAN_SUB_SECTION_ID = B.SUB_SECTION " +
                    "    AND cp.COVERAGE_PLAN_ID = B.COVERAGE_PLAN_ID " +
                    "LEFT JOIN " +
                    "    POLICY p ON p.CONTRACT_ID = B.CONTRACT_ID " +
                    "JOIN " +
                    "    POLICY_COVERAGE_VALUE pcv ON pcv.POLICY_ID = p.POLICY_ID " +
                    "JOIN " +
                    "    ( " +
                    "        SELECT " +
                    "            POLICY_ID, " +
                    "            MAX(ENDORSEMENT_ID) AS MAX_ENDORSEMENT_ID " +
                    "        FROM " +
                    "            POLICY " +
                    "        GROUP BY " +
                    "            POLICY_ID " +
                    "    ) AS max_p ON max_p.POLICY_ID = p.POLICY_ID AND max_p.MAX_ENDORSEMENT_ID = pcv.ENDORSEMENT_ID " +
                    "JOIN " +
                    "    INSURANCE_COVERAGE ic ON ic.SECTION_ID = cp.COVERAGE_PLAN_SECTION_ID " +
                    "    AND ic.SUB_SECTION_ID = cp.COVERAGE_PLAN_SUB_SECTION_ID " +
                    "    AND ic.COVERAGE_ID = pcv.IC_COVERAGE_ID " +
                    "WHERE " +
                    "    p.ENDORSEMENT_ID = pcv.ENDORSEMENT_ID " +
                    "    AND p.POLICY_ID = CONCAT(:policyPrefix, '-'', :policyNumber)";
}

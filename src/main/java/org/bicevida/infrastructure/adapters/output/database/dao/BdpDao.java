package org.bicevida.infrastructure.adapters.output.database.dao;


import io.agroal.api.AgroalDataSource;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import org.bicevida.domain.models.Coverage;
import org.bicevida.domain.ports.output.BdpOutputPort;
import org.bicevida.infrastructure.adapters.output.database.entities.CoverageEntity;
import org.bicevida.infrastructure.adapters.output.database.entities.MarkEntity;
import org.bicevida.infrastructure.adapters.output.database.entities.PlanEntity;
import org.bicevida.infrastructure.adapters.output.database.mappers.CoverageMapper;
import org.bicevida.infrastructure.adapters.output.database.utils.QueryBuilder;

import java.util.Collections;
import java.util.List;

import static org.bicevida.infrastructure.Constants.*;
import static org.bicevida.infrastructure.adapters.output.database.queries.BdpQueries.*;
import static org.bicevida.infrastructure.adapters.output.database.utils.enums.QueryType.SELECT;


@ApplicationScoped
@SuppressWarnings("unchecked")
public class BdpDao implements BdpOutputPort {

    private final AgroalDataSource agroalDataSource;

    @Inject
    public BdpDao(AgroalDataSource dataSource) {
        agroalDataSource = dataSource;
    }

    @Override
    public List<Coverage> getCollectivePolicyCoverages(Integer policyPrefix, Integer policyNumber, Integer policySequence) {

        List<CoverageEntity> policyCoverages;
        try {
            QueryBuilder queryBuilder = new QueryBuilder(this.agroalDataSource, CoverageEntity.class);
            policyCoverages = (List<CoverageEntity>) queryBuilder
                    .setQueryType(SELECT)
                    .query(GET_COLLECTIVE_POLICY_COVERAGES)
                    .addParameter(POLICY_NUMBER, policyNumber)
                    .addParameter(POLICY_PREFIX, policyPrefix)
                    .addParameter(POLICY_SEQUENCE, policySequence)
                    .getList();
        } catch (Exception e) {
            return Collections.EMPTY_LIST;
        }

        return CoverageMapper.toModels(policyCoverages);

    }

    @Override
    public List<Coverage> getIndividualPolicyCoverages(Integer policyPrefix, Integer policyNumber) {

        List<CoverageEntity> policyCoverages;
        try {
            QueryBuilder queryBuilder = new QueryBuilder(this.agroalDataSource, CoverageEntity.class);
            policyCoverages = (List<CoverageEntity>) queryBuilder
                    .setQueryType(SELECT)
                    .query(GET_INDIVIDUAL_POLICY_COVERAGE)
                    .addParameter(POLICY_NUMBER, policyNumber)
                    .addParameter(POLICY_PREFIX, policyPrefix)
                    .getList();
        } catch (Exception e) {
            return Collections.EMPTY_LIST;
        }

        return CoverageMapper.toModels(policyCoverages);
    }

    @Override
    public List<Coverage> getMark2IndividualPolicyCoverages(Integer policyPrefix, Integer policyNumber) {
        
        List<CoverageEntity> policyCoverages;
        try {
            QueryBuilder queryBuilder = new QueryBuilder(this.agroalDataSource, CoverageEntity.class);
            policyCoverages = (List<CoverageEntity>) queryBuilder
                    .setQueryType(SELECT)
                    .query(GET_MARK2_INDIVIDUAL_POLICY_COVERAGE)
                    .addParameter(POLICY_NUMBER, policyNumber)
                    .addParameter(POLICY_PREFIX, policyPrefix)
                    .getList();
        } catch (Exception e) {
            return Collections.EMPTY_LIST;
        }

        return CoverageMapper.toModels(policyCoverages);
    }

    @Override
    public List<Coverage> getMark5IndividualPolicyCoverages(Integer policyPrefix, Integer policyNumber) {
        List<CoverageEntity> policyCoverages;
        try {
            QueryBuilder queryBuilder = new QueryBuilder(this.agroalDataSource, CoverageEntity.class);
            policyCoverages = (List<CoverageEntity>) queryBuilder
                    .setQueryType(SELECT)
                    .query(GET_MARK5_INDIVIDUAL_POLICY_COVERAGE)
                    .addParameter(POLICY_NUMBER, policyNumber)
                    .addParameter(POLICY_PREFIX, policyPrefix)
                    .getList();
        } catch (Exception e) {
            return Collections.EMPTY_LIST;
        }

        return CoverageMapper.toModels(policyCoverages);
    }

    @Override
    public Long getPlanId(Integer policyNumber) {
        PlanEntity plan;
        try {
            QueryBuilder queryBuilder = new QueryBuilder(this.agroalDataSource, PlanEntity.class);
            plan = (PlanEntity) queryBuilder
                    .setQueryType(SELECT)
                    .query(GET_ID_PLAN)
                    .addParameter(POLICY_NUMBER, policyNumber)
                    .getObject();
        } catch (Exception e) {
            return 0L;
        }

        return plan.getId();
    }

    @Override
    public Long getIndividualPlusPolicyMark(Integer policyNumber) {
        MarkEntity mark;
        try {
            QueryBuilder queryBuilder = new QueryBuilder(this.agroalDataSource, MarkEntity.class);
            mark = (MarkEntity) queryBuilder
                    .setQueryType(SELECT)
                    .query(GET_POLICY_MARK)
                    .addParameter(POLICY_NUMBER, policyNumber)
                    .getObject();
        } catch (Exception e) {
            return 0L;
        }

        return mark.getId();
    }
}



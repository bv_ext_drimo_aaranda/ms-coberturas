package org.bicevida.infrastructure.adapters.output.database.queries;

public class OpenPlanQueries {
    public static final String GET_POLICY_COVERAGES =
            "SELECT " +
                    "    CASE " +
                    "        WHEN PRE.ID_PRESTACION IS NULL THEN PPB.ID_PREST_BASICA " +
                    "        ELSE PRE.ID_PRESTACION " +
                    "    END AS ID_COBERTURA, " +
                    "    CASE " +
                    "        WHEN TRIM(PRE.DESC_PRESTACION) IS NULL THEN PPS.DESC_PREST_BASICA " +
                    "        ELSE PRE.DESC_PRESTACION " +
                    "    END AS PRESTACION, " +
                    "    CASE " +
                    "        WHEN COALESCE(PPT.VALOR_UF, 0) = 0 THEN PLA.TOPE " +
                    "        ELSE PPT.VALOR_UF " +
                    "    END AS CAPITAL_ASEGURADO " +
                    "FROM " +
                    "    OPER_PLANES.PLAN_PREST_BASICA AS PPB " +
                    "LEFT JOIN " +
                    "    OPER_PLANES.PRESTACIONES_BASICAS AS PPS " +
                    "ON " +
                    "    PPB.ID_PREST_BASICA = PPS.ID_PREST_BASICA " +
                    "LEFT JOIN " +
                    "    OPER_PLANES.PLANES_PRESTACIONES_TOPES AS PPT " +
                    "ON " +
                    "    PPT.ID_PLAN_PREST_BASICA = PPB.ID_PLAN_PREST_BASICA " +
                    "LEFT JOIN " +
                    "    PRESTACIONES AS PRE " +
                    "ON " +
                    "    PRE.ID_PREST_BASICA = PPB.ID_PREST_BASICA " +
                    "    AND PRE.ID_PRESTACION = PPT.ID_PRESTACION " +
                    "INNER JOIN " +
                    "    OPER_PLANES.PLANES AS PLA " +
                    "ON " +
                    "    PLA.ID_PLAN = PPB.ID_PLAN " +
                    "WHERE " +
                    "    PPB.ID_PLAN = :planId";

}

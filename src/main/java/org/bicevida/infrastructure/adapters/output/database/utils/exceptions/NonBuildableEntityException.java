package org.bicevida.infrastructure.adapters.output.database.utils.exceptions;

import org.bicevida.application.exceptions.BaseException;

import static jakarta.ws.rs.core.Response.Status.INTERNAL_SERVER_ERROR;


public class NonBuildableEntityException extends BaseException {
    public NonBuildableEntityException(){
        super("Selected class is not able to be mapped by builder", INTERNAL_SERVER_ERROR);
    }


}

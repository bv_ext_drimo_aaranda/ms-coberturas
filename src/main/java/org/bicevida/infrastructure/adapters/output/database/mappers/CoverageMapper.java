package org.bicevida.infrastructure.adapters.output.database.mappers;


import org.bicevida.domain.models.Coverage;
import org.bicevida.domain.models.Hello;
import org.bicevida.infrastructure.adapters.output.database.entities.CoverageEntity;

import java.util.List;
import java.util.stream.Collectors;

public class CoverageMapper {

    public static Coverage toModel(CoverageEntity coverageEntity){
        if(coverageEntity == null){
            return null;
        }
        return Coverage.builder()
                .coverageTypeId(coverageEntity.getCoverageTypeId())
                .benefit(coverageEntity.getBenefit())
                .insuredAmount(coverageEntity.getInsuredAmount())
                .build();
    }


    public static List<Coverage> toModels(List<CoverageEntity> coverageEntities){
        if(coverageEntities == null){
            return null;
        }
        return coverageEntities.stream()
                .map(CoverageMapper::toModel)
                .collect(Collectors.toList());
    }


}

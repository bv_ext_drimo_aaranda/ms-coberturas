package org.bicevida.infrastructure.adapters.output.database.dao;


import io.agroal.api.AgroalDataSource;
import io.quarkus.agroal.DataSource;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import org.bicevida.domain.models.Coverage;
import org.bicevida.domain.ports.output.SysoneOutputPort;
import org.bicevida.infrastructure.adapters.output.database.entities.CoverageEntity;
import org.bicevida.infrastructure.adapters.output.database.mappers.CoverageMapper;
import org.bicevida.infrastructure.adapters.output.database.utils.QueryBuilder;

import java.util.Collections;
import java.util.List;

import static org.bicevida.infrastructure.Constants.*;
import static org.bicevida.infrastructure.adapters.output.database.queries.SysoneQueries.GET_POLICY_COVERAGES;
import static org.bicevida.infrastructure.adapters.output.database.utils.enums.QueryType.SELECT;


@ApplicationScoped
@SuppressWarnings("unchecked")
public class SysoneDao implements SysoneOutputPort {

    private final AgroalDataSource agroalDataSource;

    @Inject
    public SysoneDao(@DataSource(SYSONE_DATASOURCE) AgroalDataSource dataSource) {
        agroalDataSource = dataSource;
    }

    @Override
    public List<Coverage> getPolicyCoverages(Integer policyPrefix, Integer policyNumber) {
        List<CoverageEntity> policyCoverages;
        try {
            QueryBuilder queryBuilder = new QueryBuilder(this.agroalDataSource, CoverageEntity.class);
            policyCoverages = (List<CoverageEntity>) queryBuilder
                    .setQueryType(SELECT)
                    .query(GET_POLICY_COVERAGES)
                    .addParameter(POLICY_PREFIX, policyPrefix)
                    .addParameter(POLICY_NUMBER, policyNumber)
                    .getList();
        } catch (Exception e) {
            return Collections.EMPTY_LIST;
        }

        return CoverageMapper.toModels(policyCoverages);
    }

}



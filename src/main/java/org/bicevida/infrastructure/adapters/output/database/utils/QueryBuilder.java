package org.bicevida.infrastructure.adapters.output.database.utils;


import org.bicevida.infrastructure.adapters.output.database.utils.anotations.ColumnName;
import org.bicevida.infrastructure.adapters.output.database.utils.enums.QueryType;
import org.bicevida.infrastructure.adapters.output.database.utils.anotations.MappedEntity;
import org.bicevida.infrastructure.adapters.output.database.utils.exceptions.*;
import org.modelmapper.ModelMapper;

import javax.sql.DataSource;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.sql.*;
import java.sql.Date;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class QueryBuilder {

    private final String QUERY_PARAM_SETTER = "?";
    private final String LIST_QUERY_PARAM_DELIMITER = ", ";
    private final String QUERY_BUILDER_PARAM_SETTER = ":";
    private final DataSource dataSource;
    private final ModelMapper modelMapper;

    private Boolean isExecuted;
    private Connection connection;
    private String query;
    private QueryType queryType;
    private ResultSet resultSet;
    private CallableStatement statement;
    private Class classType;

    private Map<String, Object> insertedParameters;

    public QueryBuilder(DataSource dataSource, Class classType) throws NonBuildableEntityException, SQLException {
        this.dataSource = dataSource;
        this.classType = classType;
        this.modelMapper = new ModelMapper();
        if(classType != null &&!isBuildable(classType)) {
            throw new NonBuildableEntityException();
        }
        this.classType = classType;
        this.isExecuted = false;
        this.insertedParameters = new HashMap<>();
        this.openConnection();
    }

    public QueryBuilder(DataSource dataSource) throws Exception {
        this(dataSource, null);
    }

    private boolean isBuildable(Class classType) {
        Annotation[] annotations = classType.getDeclaredAnnotations();
        for (Annotation annotation : annotations) {
            if (annotation.annotationType().equals(MappedEntity.class)) {
                return true;
            }
        }
        return false;
    }


    private void openConnection() throws SQLException {
        this.connection = this.dataSource.getConnection();
    }

    public QueryBuilder setQueryType(QueryType queryType) {
        this.queryType = queryType;
        return this;
    }

    public QueryBuilder query( String query) throws QueryStatementException {
        if (this.queryType != null && query.toLowerCase().contains(this.queryType.getStatementName())) {
            this.query = query;
        }
        else {
            throw new QueryStatementException();
        }
        return this;
    }

    public QueryBuilder addParameter(String paramIdentifier, Object value) {
        this.insertedParameters.put(paramIdentifier, value);
        return this;
    }

    public QueryBuilder addParameter(Map<String, Object> mappedParameters) {
        this.insertedParameters.putAll(mappedParameters);
        return this;
    }

    private void checkParameters(Set<String> queryParameters) throws MissingParameterException {
        for (String queryParameter : queryParameters) {
            if (!this.insertedParameters.containsKey(queryParameter)) {
                throw new MissingParameterException();
            }
        }
    }


    private void setParam(int index, Object value) throws SQLException {

        if (String.class.equals(value.getClass())) {
            this.statement.setString(index, (String) value);
        } else if (Long.class.equals(value.getClass())) {
            this.statement.setLong(index, (Long) value);
        } else if (Integer.class.equals(value.getClass())) {
            this.statement.setInt(index, (Integer) value);
        } else if (Boolean.class.equals(value.getClass())) {
            this.statement.setBoolean(index, (Boolean) value);
        } else if (Date.class.equals(value.getClass())) {
            this.statement.setTimestamp(index, new Timestamp(((Date) value).getTime()));
        } else {
            this.statement.setObject(index,value);
        }

    }

    public void setQueryParamsPlaceholders(Set<String> uniqueParameters){

        for (String uniqueParam : uniqueParameters) {
            Object parameterValue = this.insertedParameters.get(uniqueParam);
            if (parameterValue instanceof List){
                int listSize = ((List<?>) parameterValue).size();
                this.query = this.query.replaceAll(
                        QUERY_BUILDER_PARAM_SETTER+uniqueParam,
                        String.join(LIST_QUERY_PARAM_DELIMITER,
                                Collections.nCopies(listSize, QUERY_PARAM_SETTER)
                        )
                );
            }
            else{
                this.query = this.query.replaceAll(QUERY_BUILDER_PARAM_SETTER+uniqueParam, QUERY_PARAM_SETTER);
            }
        }
    }

    private void setParams() throws SQLException, MissingParameterException {

        List<String> orderedQueryParameters = new ArrayList<>();
        Matcher paramsMatcher = Pattern.compile(
                String.format("(?<!%s)%s([a-zA-Z]+)",
                        QUERY_BUILDER_PARAM_SETTER,
                        QUERY_BUILDER_PARAM_SETTER))
                .matcher(this.query);
        while (paramsMatcher.find()) {
            orderedQueryParameters.add(paramsMatcher.group().substring(1));
        }

        Set<String> uniqueParameters = new HashSet<>(orderedQueryParameters);
        this.checkParameters(uniqueParameters);
        this.setQueryParamsPlaceholders(uniqueParameters);
        this.statement = this.connection.prepareCall(this.query);

        int index = 1;
        for (String orderedQueryParameter : orderedQueryParameters){
            Object parameterValue = this.insertedParameters.get(orderedQueryParameter);
            if(parameterValue instanceof List<?>){
                for (Object listedValue : (List<?>) parameterValue){
                    this.setParam(index, listedValue);
                    index++;
                }
            }
            else {
                this.setParam(index, parameterValue);
                index++;
            }
        }
    }

    public QueryBuilder execute() throws SQLException, MissingParameterException {
        if (this.connection == null || this.connection.isClosed()) {
            this.openConnection();
        }
        this.setParams();
        if (this.queryType.equals(QueryType.SELECT)) {
            this.resultSet = this.statement.executeQuery();
        }
        else {
            this.statement.executeUpdate();
        }
        this.isExecuted = true;
        return this;
    }

    private Map<String, Object> getMappedObject() throws SQLException {
        Map<String, Object> mappedResult = new HashMap<>();
        Field[] classFields = classType.getDeclaredFields();
        for (Field classField : classFields) {
            if (classField.isAnnotationPresent(ColumnName.class)) {
                ColumnName annotation = classField.getAnnotation(ColumnName.class);
                try {
                    if (String.class.equals(classField.getType())) {
                        mappedResult.put(classField.getName(), this.resultSet.getString(annotation.value()));
                    } else if (Long.class.equals(classField.getType())) {
                        mappedResult.put(classField.getName(), this.resultSet.getLong(annotation.value()));
                    } else if (Integer.class.equals(classField.getType())) {
                        mappedResult.put(classField.getName(), this.resultSet.getInt(annotation.value()));
                    } else if (Boolean.class.equals(classField.getType())) {
                        mappedResult.put(classField.getName(), this.resultSet.getBoolean(annotation.value()));
                    } else if (Date.class.equals(classField.getType())) {
                        mappedResult.put(classField.getName(), this.resultSet.getDate(annotation.value()));
                    }
                } catch (SQLException e) {
                    mappedResult.put(classField.getName(), null);
                }
            }
        }
        return mappedResult;
    }

    private void checkQueryType() throws MissingQueryTypeException {
        if (this.queryType == null) {
         throw new MissingQueryTypeException();
        }
    }

    private void executeIfNotExecuted() throws SQLException, MissingParameterException {
        if(!this.isExecuted){
            this.execute();
        }
    }

    public Object getObject() throws NonTransactionalException, SQLException, MissingQueryTypeException, MissingParameterException {

        this.executeIfNotExecuted();
        if (this.queryType.equals(QueryType.SELECT)) {
            this.resultSet.next();
            Object result = this.modelMapper.map(getMappedObject(), this.classType);
            this.close();
            return result;
        }
        else {
            throw new NonTransactionalException();
        }
    }

    public List<?> getList() throws SQLException, NonTransactionalException, MissingParameterException {
        this.executeIfNotExecuted();
        if (this.queryType.equals(QueryType.SELECT)) {
            List<Object> resultSet = new ArrayList<>();
            while (this.resultSet.next()) {
                resultSet.add(this.modelMapper.map(getMappedObject(), this.classType));
            }
            this.close();
            return resultSet;
        }
        else {
            throw new NonTransactionalException();
        }
    }

    public void close() throws SQLException {

        if (this.resultSet != null) {
            this.resultSet.close();
        }
        if (this.statement != null) {
            this.statement.close();
        }
        if (this.connection != null) {
            this.connection.close();
        }
        this.insertedParameters.clear();
    }

}

package org.bicevida.infrastructure.adapters.output.database.utils.exceptions;

import org.bicevida.application.exceptions.BaseException;

import static jakarta.ws.rs.core.Response.Status.INTERNAL_SERVER_ERROR;


public class MissingParameterException extends BaseException {
    public MissingParameterException(){
        super("Some parameters are missing", INTERNAL_SERVER_ERROR);
    }


}

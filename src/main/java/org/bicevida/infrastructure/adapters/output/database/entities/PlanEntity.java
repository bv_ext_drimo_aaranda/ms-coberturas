
package org.bicevida.infrastructure.adapters.output.database.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.bicevida.infrastructure.adapters.output.database.utils.anotations.ColumnName;
import org.bicevida.infrastructure.adapters.output.database.utils.anotations.MappedEntity;

@Setter
@Getter
@NoArgsConstructor
@MappedEntity
public class PlanEntity {

    @ColumnName(value = "planId")
    private Long id;

}

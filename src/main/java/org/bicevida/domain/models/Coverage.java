package org.bicevida.domain.models;

import lombok.*;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Coverage {
    private Long coverageTypeId;
    private String benefit;
    private Double insuredAmount;
}

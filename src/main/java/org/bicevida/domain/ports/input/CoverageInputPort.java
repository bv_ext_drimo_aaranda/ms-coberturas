package org.bicevida.domain.ports.input;

public interface CoverageInputPort {
    public Object getPolicyCoverages(Integer policyNumber, Integer policyPrefix, Integer policySequence);
}

package org.bicevida.domain.ports.output;

import org.bicevida.domain.models.Coverage;

import java.util.List;

public interface BdpOutputPort {
    public List<Coverage> getCollectivePolicyCoverages(Integer policyPrefix, Integer policyNumber, Integer policySequence);
    public List<Coverage> getIndividualPolicyCoverages(Integer policyPrefix, Integer policyNumber);
    public List<Coverage> getMark2IndividualPolicyCoverages(Integer policyPrefix, Integer policyNumber);
    public List<Coverage> getMark5IndividualPolicyCoverages(Integer policyPrefix, Integer policyNumber);
    public Long getPlanId(Integer policyNumber);
    public Long getIndividualPlusPolicyMark(Integer policyNumber);
}

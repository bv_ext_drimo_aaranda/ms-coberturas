package org.bicevida.domain.ports.output;

import org.bicevida.domain.models.Coverage;

import java.util.List;

public interface SysoneOutputPort {
    public List<Coverage> getPolicyCoverages(Integer policyPrefix, Integer policyNumber);
}

package org.bicevida.domain.ports.output;

import org.bicevida.domain.models.Coverage;

import java.util.List;

public interface OpenPlanOutputPort {
    public List<Coverage> getPolicyCoverages(Long planId);
}
